FROM python:3.7

ENV PYTHONUNBUFFERED 1

RUN mkdir /bot-template

WORKDIR /bot-template

ADD . /bot-template/

RUN pip install -r requirements.txt

ENTRYPOINT ["python3", "main.py"]
