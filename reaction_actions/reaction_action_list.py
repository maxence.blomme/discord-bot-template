class ReactionActionList:
    actions = []

    @staticmethod
    def add_action(action):
        ReactionActionList.actions.append(action)
